# Task Manager

## DEVELOPER INFO

* NAME: Kolesnik Nikolay

* E-MAIL: kolesnik.nik.vrn@gmail.com

* E-MAIL: kolesnik15121992@gmail.com

## SOFTWARE

* OpenJDK 8

* Intellij Idea

* MS Windows 10

## HARDWARE

* **RAM**: 16Gb

* **CPU**: i5

* **HDD**: 128Gb

## RUN PROGRAMM

```shell
java -jar "Task Manager.jar"
```
